# terraform {
#   required_providers {
#     google = {
#       source = "hashicorp/google"
#       version = "4.47.0"
#     }
#   }
# }

module "gke" {
  source                 = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version                = "24.1.0"
  project_id             = var.gcp_project_id
  name                   = "cluster-${var.gcp_instance_name}-${var.gcp_env_name}"
  regional               = true
  region                 = var.gcp_region
  network                = module.gcp-network.network_name
  subnetwork             = module.gcp-network.subnets_names[0]
  ip_range_pods          = var.gce_ip_range_name
  ip_range_services      = var.gce_ip_range_services_name
  
  node_pools = [
    {
      name                      = "nodepool-${var.gcp_instance_name}"
      machine_type              = var.gcp_machine_type
      node_locations            = var.gcp_locations
      min_count                 = 2
      max_count                 = 3
      disk_size_gb              = 30
    },
  ]
}

data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

module "gke_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  version = "24.1.0"
  # depends_on   = [module.gke]
  project_id   = var.gcp_project_id
  location     = module.gke.location
  cluster_name = module.gke.name
}

resource "local_file" "kubeconfig" {
  content  = module.gke_auth.kubeconfig_raw
  filename = "config"
  # filename = "C:/Users/Dmitry/.kube/kubeconfig-${var.gcp_env_name}"
}

module "gcp-network" {
  source       = "terraform-google-modules/network/google"
  version      = "6.0.0"
  project_id   = var.gcp_project_id
  network_name = "${var.gcp_network}-${var.gcp_env_name}"

  subnets = [
    {
      subnet_name   = "${var.gcp_subnetwork}-${var.gcp_env_name}"
      subnet_ip     = "10.10.0.0/16"
      subnet_region = var.gcp_region
    },
  ]

  secondary_ranges = {
    "${var.gcp_subnetwork}-${var.gcp_env_name}" = [
      {
        range_name    = var.gce_ip_range_name
        ip_cidr_range = "10.20.0.0/16"
      },
      {
        range_name    = var.gce_ip_range_services_name
        ip_cidr_range = "10.30.0.0/16"
      },
    ]
  }
}
