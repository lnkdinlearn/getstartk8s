# GCP Provider

provider "google" {
  credentials = var.gcp_svc_key
  project = var.gcp_project_id
  region = var.gcp_region
  zone = var.gcp_locations
}
