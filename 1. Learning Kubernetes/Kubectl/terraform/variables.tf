variable "gcp_svc_key" {
  description = "Key for GCP in json format"
  default = "C:/Users/Dmitry/.gcp/test-jira-project-400509-ec0c63b94e5c.json"
}

variable "gcp_project_id" {
  description = "The project ID to host the cluster in"
  default = "test-jira-project-400509"
}

variable "gcp_env_name" {
  description = "The environment for the GCE cluster"
  default     = "prod"
}

variable "gcp_region" {
  description = "The region to host the cluster in"
  default     = "us-east1"
}

variable "gcp_machine_type" {
  description = "Type of instances"
  default = "e2-micro"
}

variable "gcp_disk_size" {
  description = "Size of disk space"
  default = "10"
}
variable "gcp_locations" {
  description = "The nodes locations to host the pods in"
  default = "us-east1-b" # us-east1-c, us-east1-d"
}

variable "gcp_vpc_network_name" {
  description = "VPC network for our serveces"
  default = "test-vpc"
}

variable "gce_ssh_user" {
  description = "User to instance connect"
  default = "ubuntu"
}

variable "gce_ssh_pub_key_file" {
  description = "SSH key to connect instances"
  default = "C:/Users/Dmitry/.gcp/.gcptest.pub"
}

variable "gce_ssh_private_key" {
  description = "SSH private key to connect instances"
  default = "C:/Users/Dmitry/.gcp/.gcptest"
} 

variable "gcp_network" {
  description = "The VPC network created to host the cluster in"
  default     = "test-network"
}

variable "gcp_subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "test-subnet"
}

variable "gce_ip_range_name" {
  description = "The secondary ip range to use for pods"
  default     = "test-ip-range"
}

variable "gce_ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}

variable "gcp_instance_name" {
  description = "Instance names"
  default = "test"
}

variable "gce_type_of_image" {
  description = "Image OS type version"
  default = "projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-v20230918"
}
